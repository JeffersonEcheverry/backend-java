package com.recobro.incapacidades;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IncapacidadesApplication {

	public static void main(String[] args) {
		SpringApplication.run(IncapacidadesApplication.class, args);
	}

}
