package com.recobro.incapacidades.controller;

import com.recobro.incapacidades.facade.Facade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/recobro")
@Api(tags = "recobro")
public class RecobroController {

    @Autowired
    private Facade facade;

    @PostMapping("/crearRecobro")
    @ApiOperation(value = "Crear Recobro", notes = "Este metodo crea un recobro")
    @ApiResponses({@ApiResponse(code = 200, message = "Recobro Creado Exitosamente"),
            @ApiResponse(code = 400, message = "Data Invalida"),
            @ApiResponse(code = 500, message = "Error Interno")})
    public String crearRecobro(String json){
        return facade.operacion(json);
    }
}
