/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.controller;

import com.recobro.incapacidades.facade.Facade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeffersonecheverry
 */
@CrossOrigin
@RestController
@RequestMapping("/incapacidad")
@Api(tags = "incapacidad")
public class IncapacidadesController {
    
    @Autowired
    private Facade facade;
    
    @PostMapping("/crearIncapacidad")
    @ApiOperation(value = "Crear Incapacidad", httpMethod = "POST", notes = "Este metodo crea una incapacidad")
    @ApiResponses({@ApiResponse(code = 200, message = "Incapacidad Creada Exitosamente"),
                   @ApiResponse(code = 400, message = "Data Invalida"),
                   @ApiResponse(code = 500, message = "Error Interno")})
    public String crearIncapacidad(String json){
        return facade.operacion(json);
    }
    
}
