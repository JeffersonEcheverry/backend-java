package com.recobro.incapacidades.controller;

import com.recobro.incapacidades.facade.Facade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/trabajador")
@Api(tags = "Trabajador")
public class TrabajadorController {

    @Autowired
    private Facade facade;

    @PostMapping("/crearTrabajador")
    @ApiOperation(value = "Crear Trabajador", notes = "Este metodo crea un Trabajador")
    @ApiResponses({@ApiResponse(code = 200, message = "Trabajador Creado Exitosamente"),
            @ApiResponse(code = 400, message = "Data Invalida"),
            @ApiResponse(code = 500, message = "Error Interno")})
    public String crearTrabajador(String json){
        return facade.operacion(json);
    }
}
