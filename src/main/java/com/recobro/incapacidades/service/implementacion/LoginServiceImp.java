/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.service.implementacion;

import com.recobro.incapacidades.entities.Usuario;
import com.recobro.incapacidades.modelo.UsuarioModelo;
import com.recobro.incapacidades.repository.UsuarioRepository;
import com.recobro.incapacidades.security.JwtUtil;
import com.recobro.incapacidades.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jeffersonecheverry
 */
@Service("loginService")
public class LoginServiceImp implements LoginService {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private JwtUtil jwtUtil;
    
    @Value("${usuario.no.logeado}")
    private String mensajeNoLogeado;
    
    @Value("${usuario.dato.corrupto}")
    private String mensaeDatoCorrupto;
    
    @Override
    @Transactional
    public String loginUsuario(UsuarioModelo usuarioModelo) throws Exception {
        if(usuarioModelo != null){
            Usuario usuario = usuarioRepository.findUsuario(usuarioModelo.getCorreo(), usuarioModelo.getPassword());
            if(usuario != null){
                return jwtUtil.generateToken(usuarioModelo);
            }else{
                return mensajeNoLogeado;
            }
        }
        return mensaeDatoCorrupto;
    }
    
    
    
}
