package com.recobro.incapacidades.service.implementacion;

import com.recobro.incapacidades.entities.Trabajador;
import com.recobro.incapacidades.modelo.TrabajadorModelo;
import com.recobro.incapacidades.repository.TrabajadorRepository;
import com.recobro.incapacidades.service.TrabajadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("trabajadorService")
public class TrabajadorServiceImp implements TrabajadorService {

    @Value("${EXITO}")
    private Integer exito;

    @Autowired
    private TrabajadorRepository trabajadorRepository;

    @Override
    public TrabajadorModelo crearTrabajador(TrabajadorModelo trabajadorModelo) throws Exception {
        Trabajador trabajador = trabajadorRepository.encontrarTrabajador(trabajadorModelo.getTbId());
        if(trabajador == null){
            if(trabajadorRepository.crearTrabajador(trabajadorModelo.getNombre(), trabajadorModelo.getApellido(), trabajadorModelo.getEdad(), trabajadorModelo.getSalario().toString(), trabajadorModelo.getEmpresa(), trabajadorModelo.getEntidad()) == exito){
                return trabajadorModelo;
            }
        } else {
            if(trabajadorRepository.actualizarTrabajador(trabajadorModelo.getNombre(), trabajadorModelo.getApellido(), trabajadorModelo.getEdad(), trabajadorModelo.getSalario().toString(), trabajadorModelo.getEmpresa(), trabajadorModelo.getEntidad(), trabajadorModelo.getTbId()) == exito){
                return trabajadorModelo;
            }
        }
        return null;
    }
}
