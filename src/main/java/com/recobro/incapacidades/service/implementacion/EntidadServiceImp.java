package com.recobro.incapacidades.service.implementacion;

import com.recobro.incapacidades.entities.Entidad;
import com.recobro.incapacidades.modelo.EntidadModelo;
import com.recobro.incapacidades.repository.EntidadRepository;
import com.recobro.incapacidades.service.EntidadService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("entidadService")
public class EntidadServiceImp implements EntidadService {

        @Value("${EXITO}")
        private Integer exito;

        @Autowired
        private EntidadRepository entidadRepository;

    @Override
    @Transactional
    public EntidadModelo crearEntidad(EntidadModelo entidadModelo) throws Exception {
        Entidad entidad = entidadRepository.encontrarEntidad(entidadModelo.getEpsId());
        if(entidad == null){
            if(entidadRepository.crearEntidad(entidadModelo.getNombre(), entidadModelo.getDescripcion(), entidadModelo.getPago()) == exito){
                return entidadModelo;
            }
        } else {
            if(entidadRepository.actualizarEntidad(entidadModelo.getNombre(), entidadModelo.getDescripcion(), entidadModelo.getPago()) == exito){
                return entidadModelo;
            }
        }
        return null;
    }
}
