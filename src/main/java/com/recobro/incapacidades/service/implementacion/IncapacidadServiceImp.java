/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.service.implementacion;

import com.recobro.incapacidades.entities.Incapacidad;
import com.recobro.incapacidades.modelo.IncapacidadModelo;
import com.recobro.incapacidades.repository.IncapacidadRepository;
import com.recobro.incapacidades.service.IncapacidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jeffersonecheverry
 */
@Service("incapacidadService")
public class IncapacidadServiceImp implements IncapacidadService{
    
    @Value("${EXITO}")
    private int exito;
    
    @Autowired
    private IncapacidadRepository incapacidadRepository;
    
    
    @Override
    @Transactional
    public IncapacidadModelo crearIncapacidad(IncapacidadModelo incapacidadModelo) throws Exception {
        Incapacidad incapacidad = incapacidadRepository.encontrarIncapacidad(incapacidadModelo.getIncapId());
        if(incapacidad == null){
            if(incapacidadRepository.crearIncapacidad(incapacidadModelo.getTipoIncapacidad(), incapacidadModelo.getDescripcion(), incapacidadModelo.getAuxilio()) == exito){
                return incapacidadModelo;
            }
        }else{
            if(incapacidadRepository.actualizarIncapacidad(incapacidadModelo.getTipoIncapacidad(), incapacidadModelo.getDescripcion(), incapacidadModelo.getAuxilio(), incapacidadModelo.getIncapId()) == exito){
                return incapacidadModelo;
            }    
        }
        return null;
    }
}