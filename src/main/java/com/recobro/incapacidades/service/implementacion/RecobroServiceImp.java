package com.recobro.incapacidades.service.implementacion;

import com.recobro.incapacidades.entities.Recobro;
import com.recobro.incapacidades.modelo.RecobroModelo;
import com.recobro.incapacidades.repository.RecobroRepository;
import com.recobro.incapacidades.service.RecobroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("recobroService")
public class RecobroServiceImp implements RecobroService {

    @Value("${EXITO}")
    private Integer exito;

    @Autowired
    private RecobroRepository recobroRepository;

    @Override
    public RecobroModelo crearRecobro(RecobroModelo recobroModelo) throws Exception {
        Recobro recobro = recobroRepository.encontrarRecobro(recobroModelo.getRecoId());
        if(recobro == null){
            if(recobroRepository.crearRecobro(recobroModelo.getDescripcion(), recobroModelo.getRemuneracion(), recobroModelo.getIncapacidad(), recobroModelo.getTrabajador(), recobroModelo.getEntidad()) == exito){
                return recobroModelo;
            }
        } else {
            if (recobroRepository.actualizarRecobro(recobroModelo.getDescripcion(), recobroModelo.getRemuneracion(), recobroModelo.getIncapacidad(), recobroModelo.getTrabajador(), recobroModelo.getEntidad(), recobroModelo.getRecoId()) == exito){
                return recobroModelo;
            }
        }
        return null;
    }
}
