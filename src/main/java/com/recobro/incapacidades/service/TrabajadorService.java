package com.recobro.incapacidades.service;

import com.recobro.incapacidades.modelo.TrabajadorModelo;

public interface TrabajadorService {

    TrabajadorModelo crearTrabajador(TrabajadorModelo trabajadorModelo) throws Exception;
}