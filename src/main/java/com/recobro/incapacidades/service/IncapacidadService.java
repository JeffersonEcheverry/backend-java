/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.service;

import com.recobro.incapacidades.modelo.IncapacidadModelo;

/**
 *
 * @author jeffersonecheverry
 */
public interface IncapacidadService {
    
    IncapacidadModelo crearIncapacidad(IncapacidadModelo incapacidadModelo) throws Exception;
    
}
