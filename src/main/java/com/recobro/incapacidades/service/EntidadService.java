package com.recobro.incapacidades.service;

import com.recobro.incapacidades.modelo.EntidadModelo;

public interface EntidadService {

    EntidadModelo crearEntidad(EntidadModelo entidadModelo) throws Exception;
}