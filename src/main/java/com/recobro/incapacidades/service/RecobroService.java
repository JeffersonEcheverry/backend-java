package com.recobro.incapacidades.service;

import com.recobro.incapacidades.modelo.RecobroModelo;

public interface RecobroService {

    RecobroModelo crearRecobro (RecobroModelo recobroModelo) throws Exception;
}
