package com.recobro.incapacidades.modelo;

import lombok.Data;

@Data
public class EntidadModelo {

    private Integer epsId;
    private String nombre;
    private String descripcion;
    private Integer pago;

}
