package com.recobro.incapacidades.modelo;

import lombok.Data;

@Data
public class RecobroModelo {

    private Integer recoId;
    private String descripcion;
    private Integer remuneracion;
    private Integer incapacidad;
    private Integer trabajador;
    private Integer entidad;

}
