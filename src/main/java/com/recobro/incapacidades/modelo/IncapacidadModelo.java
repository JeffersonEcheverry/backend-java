/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.modelo;

import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Data
public class IncapacidadModelo {

    private Integer incapId;
    private String tipoIncapacidad;
    private String descripcion;
    private Integer auxilio;

}