package com.recobro.incapacidades.modelo;

import lombok.Data;

@Data
public class TrabajadorModelo {

    private Integer tbId;
    private String nombre;
    private String apellido;
    private Integer edad;
    private Integer salario;
    private String empresa;
    private Integer entidad;

}
