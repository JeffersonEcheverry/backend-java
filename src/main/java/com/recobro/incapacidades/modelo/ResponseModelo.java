/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.modelo;

import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Data
public class ResponseModelo <T> {
    
    private int codigoRetorno;
    
    private String mensaje;
    
    private T body;
    
    public ResponseModelo(T body){
        this.body = body;
    }
    
}
