/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.converter;

import com.recobro.incapacidades.entities.Usuario;
import com.recobro.incapacidades.modelo.UsuarioModelo;
import org.springframework.stereotype.Component;

/**
 *
 * @author jeffersonecheverry
 */
@Component("usuarioConverter")
public class UsuarioConverter {
    
    public UsuarioModelo converterUsuarioToUsuarioModelo(Usuario usuario){
        UsuarioModelo usuarioModelo = new UsuarioModelo();
        usuarioModelo.setCorreo(usuario.getCorreo() != null ? usuario.getCorreo() : "");
        usuarioModelo.setPassword(usuario.getPassword() != null ? usuario.getPassword() : "");
        usuarioModelo.setAdministrador(usuario.getAdministrador().getId() > 0 ? usuario.getAdministrador().getId() : 0);
        
        return usuarioModelo;
    }
    
}
