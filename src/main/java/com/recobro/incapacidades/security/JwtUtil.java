/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.security;

import com.recobro.incapacidades.modelo.UsuarioModelo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author jeffersonecheverry
 */
@Component("jwtUtil")
public class JwtUtil {
    
    @Value("${jwt.secret}")
    private String secret;
    
    @Value("${token.validate}")
    private long tokenvalidate;
    
    public String getUsernameFromToken(String token) throws IOException {
        return getClaimFromToken(token, Claims::getSubject);
    }
     
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) throws IOException {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }
    
    private Claims getAllClaimsFromToken(String token) throws IOException {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }
    
    public Date getExpirationDateFromToken(String token) throws IOException {
        return getClaimFromToken(token, Claims::getExpiration);
    }
    
    private Boolean isTokenExpired(String token) throws IOException {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }
    
    public String generateToken(UsuarioModelo usuarioModelo) throws IOException {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, usuarioModelo.getCorreo());
    }
    
    public String doGenerateToken(Map<String, Object> claims, String subject) throws IOException {
        return Jwts.builder()
                .setId("softtekJWT")
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + tokenvalidate * 1000))
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }
    
    public Boolean validateToken(String token, UsuarioModelo usuarioModelo) throws IOException {
        final String username = getUsernameFromToken(token);
        return (username.equals(usuarioModelo.getCorreo()) && !isTokenExpired(token));
    }
    
}
