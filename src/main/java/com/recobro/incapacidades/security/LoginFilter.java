/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.recobro.incapacidades.modelo.UsuarioModelo;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 *
 * @author jeffersonecheverry
 */
public class LoginFilter extends AbstractAuthenticationProcessingFilter {

    public LoginFilter(String url, AuthenticationManager authenticationManager){
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authenticationManager);
    }
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        InputStream body =  request.getInputStream();
        UsuarioModelo usuarioModelo = new ObjectMapper().readValue(body, UsuarioModelo.class);
        
        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        usuarioModelo.getCorreo(), 
                        usuarioModelo.getPassword(), 
                        Collections.emptyList())
        );
    }
    
}
