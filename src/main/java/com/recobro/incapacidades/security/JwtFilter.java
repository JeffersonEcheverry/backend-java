/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.security;

import com.recobro.incapacidades.converter.UsuarioConverter;
import com.recobro.incapacidades.entities.Authority;
import com.recobro.incapacidades.entities.Usuario;
import com.recobro.incapacidades.modelo.UsuarioModelo;
import com.recobro.incapacidades.repository.AuthorityRepository;
import com.recobro.incapacidades.repository.UsuarioRepository;
import io.jsonwebtoken.ExpiredJwtException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * @author jeffersonecheverry
 */
@Component("jwtFilter")
public class JwtFilter extends OncePerRequestFilter {
    
    @Autowired
    @Qualifier("authorityRepository")
    private AuthorityRepository authorityRepository;

    @Autowired
    @Qualifier("jwtUtil")
    private JwtUtil jwtUtil;
    
    @Autowired
    @Qualifier("usuarioRepository")
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    @Qualifier("usuarioConverter")
    private UsuarioConverter usuarioConverter;
    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        
        final String requestTokenHeader = request.getHeader("Authorization");
        String username = null;
        String token = null;
        System.out.println("The token is "+requestTokenHeader);
         if(requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")){
            token = requestTokenHeader.substring(7);
            try{
                username = jwtUtil.getUsernameFromToken(token);
            }catch(IllegalArgumentException e){
                System.out.println("Unable to get token");
            }catch(ExpiredJwtException e){
                System.out.println("Token has expired");
            }
        }else{
            System.out.println("Token does not begin with Bearer String");
        }
         
        if(username != null && SecurityContextHolder.getContext().getAuthentication() == null){
            Usuario usuario = null;
            try{
                usuario = usuarioRepository.findUsuarioByCorreo(username);
            }catch(Exception e){
                
            }
            if(usuario != null){
                UsuarioModelo usuarioModelo = usuarioConverter.converterUsuarioToUsuarioModelo(usuario);
                if(jwtUtil.validateToken(token, usuarioModelo)){
                    System.out.println("The token is validate");
                    System.out.println("Username is: "+username);
                    List<Authority> authorities =  authorityRepository.authoritiesForUser(usuarioModelo.getAdministrador());
                    List<GrantedAuthority> grantedAuthoritys = authorities.stream().map(a -> new SimpleGrantedAuthority((a.getRol()))).collect(Collectors.toList());
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(usuarioModelo, username, grantedAuthoritys);
                    usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }  
            }
                   
        }
        System.out.println("Paso el filtro");
        filterChain.doFilter(request, response);
    }

    
}
