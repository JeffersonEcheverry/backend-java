/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.entities;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Entity
@Table(name = "administrador")
@Data
public class Administrador implements Serializable {
    
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name = "nombre", nullable = false)
    private String nombre;
    
    @Column(name = "apellido", nullable = false)
    private String apellido;
    
    @Column(name = "documento", nullable = false)
    private String documento;
    
    @OneToOne(mappedBy = "administrador")
    private Usuario usuario;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "authorities",
            joinColumns = {@JoinColumn(name = "administrador", referencedColumnName = "id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "authority", referencedColumnName = "id", nullable = false)}
    )
    private Set<Authority> authorities;
    
}
