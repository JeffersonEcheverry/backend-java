package com.recobro.incapacidades.entities;

import java.io.Serializable;
import java.util.Set;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "trabajador")
@Data
public class Trabajador implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "edad")
    private Integer edad;

    @Column(name = "salario")
    private Integer salario;

    @Column(name = "empresa")
    private String empresa;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_entidad_id", referencedColumnName = "id", nullable = false)
    private Entidad entidad;

    @OneToMany(mappedBy = "trabajador")//atributo dentro de la clase incapacidad
    private Set<Incapacidad> incapacidades;//Se utiliza una coleccion o listado
    
    @OneToOne(mappedBy = "trabajador")
    private Recobro recobro;
}
