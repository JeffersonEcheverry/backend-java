package com.recobro.incapacidades.entities;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "entidad")
@Data
public class Entidad implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "descripcion", nullable = false)
    private String descripcion;

    @Column(name = "pago", nullable = false)
    private Integer pago;
    
    @OneToOne(mappedBy = "entidad")
    private Recobro recobro;
    
    @OneToOne(mappedBy = "entidad")
    private Trabajador trabajador;
}
