package com.recobro.incapacidades.entities;

import java.io.Serializable;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "recobro")
@Data
public class Recobro implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "remuneracion")
    private Integer remuneracion;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_incapacidad_id", referencedColumnName = "id")
    private Incapacidad incapacidad;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_trabajador_id", referencedColumnName = "id")
    private Trabajador trabajador;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_entidad_id", referencedColumnName = "id")
    private Entidad entidad;
}