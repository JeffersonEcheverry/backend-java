/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.entities;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Entity
@Table(name = "incapacidad")
@Data
public class Incapacidad implements Serializable {
    
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name = "tipoIncapacidad", nullable = false)
    private String tipoIncapacidad;

    @Column(name = "descripcion", nullable = false)
    private String descripcion;

    @Column(name = "auxilio", nullable = false)
    private Integer auxilio;
    
    @OneToOne(mappedBy = "incapacidad")
    private Recobro recobro;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_trabajador_id", referencedColumnName = "id", nullable = false)
    private Trabajador trabajador;
}