/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.repository;

import com.recobro.incapacidades.entities.Authority;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jeffersonecheverry
 */
@Repository("authorityRepository")
public interface AuthorityRepository extends JpaRepository<Authority, Object> {
    
    @Query(value = "select a.id, a.rol from authorities au "
            + "INNER JOIN authority a ON au.authority = a.id "
            + "where au.administrador =:administrador",
            nativeQuery = true)
    public List<Authority> authoritiesForUser(@Param("administrador") Integer administrador);
    
    @Query(value = "select * from authority",
            nativeQuery = true)
    List<Authority> findAllAuthority() throws SQLException, DataAccessException;
    
    @Modifying(clearAutomatically = true)
    @Query(value = "insert into authorities (administrador, authority) VALUES (:administrador, :authority)",
            nativeQuery = true)
    int saveAuthorityForPersona(@Param("administrador") Integer administrador,
                                @Param("authority") Integer authority) throws SQLException, DataAccessException;
    
    @Modifying(clearAutomatically = true)
    @Query(value = "delete from authorities where administrador =:administrador",
            nativeQuery = true)
    int deleteAuthorityForPersona(@Param("administrador") Integer administrador) throws SQLException, DataAccessException;
}
