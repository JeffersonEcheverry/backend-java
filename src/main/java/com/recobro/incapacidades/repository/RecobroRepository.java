package com.recobro.incapacidades.repository;

import com.recobro.incapacidades.entities.Recobro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("recobroRepository")
public interface RecobroRepository extends JpaRepository<Recobro, Object> {

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "insert into recobro (id, descripcion, remuneracion, fk_incapacidad_id, fk_trabajador_id, fk_entidad_id) values (:descripcion, :remuneracion, :incapacidad, :trabajador, :entidad)",
            nativeQuery = true)
    int crearRecobro(@Param("descripcion") String descriocion,
                        @Param("remuneracion") Integer remuneracion,
                        @Param("incapacidad") Integer incapacidad,
                        @Param("trabajador") Integer trabajador,
                        @Param("entidad") Integer entidad) throws Exception;

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "update recobro set descripcion =:descripcion, remuneracion =:remuneracion, fk_incapacidad_id =:incapacidad, fk_trabajador_id =:trabajador, fk_entidad_id =:entidad where id =:id",
            nativeQuery = true)
    int actualizarRecobro(@Param("descripcion") String descripcion,
                             @Param("remuneracion") Integer remuneracion,
                             @Param("incapacidad") Integer incapacidad,
                             @Param("trabajador") Integer trabajador,
                             @Param("entidad") Integer entidad,
                             @Param("id") Integer id) throws Exception;

    @Query(value = "select * from recobro where id =:id",
            nativeQuery = true)
    Recobro encontrarRecobro(@Param("id") Integer id) throws Exception;

}
