package com.recobro.incapacidades.repository;

import com.recobro.incapacidades.entities.Entidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("entidadRepository")
public interface EntidadRepository extends JpaRepository<Entidad, Object> {

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "insert into entidad (nombre, descripcion, pago) values (:nombre, :descripcion, :pago)",
           nativeQuery = true)
    int crearEntidad(@Param("nombre") String nombre,
                     @Param("descripcion") String descripcion,
                     @Param("pago") Integer pago) throws Exception;

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "update entidad set nombre =:nombre, descripcion =:descripcion, pago =:pago",
            nativeQuery = true)
    int actualizarEntidad(@Param("nombre") String nombre,
                          @Param("descripcion") String descripcion,
                          @Param("pago") Integer pago) throws Exception;

    @Query(value = "select * from entidad where id =: id",
            nativeQuery = true)
    Entidad encontrarEntidad(@Param("id") Integer id) throws Exception;
}