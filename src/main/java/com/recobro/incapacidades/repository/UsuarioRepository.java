/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.repository;

import com.recobro.incapacidades.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jeffersonecheverry
 */
@Repository("usuarioRepository")
public interface UsuarioRepository extends JpaRepository<Usuario, Object>{
    
    @Query(value = "select * from usuario where correo =:correo and password =:password",
            nativeQuery = true)
    Usuario findUsuario(@Param("correo") String correo,
                        @Param("password") String password) throws Exception;
    
    @Query(value = "select * from usuario where correo =:correo",
            nativeQuery = true)
    Usuario findUsuarioByCorreo(@Param("correo") String correo) throws Exception;
    
}
