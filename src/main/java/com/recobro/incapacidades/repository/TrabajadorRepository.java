package com.recobro.incapacidades.repository;

import com.recobro.incapacidades.entities.Trabajador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("trabajador")
public interface TrabajadorRepository extends JpaRepository<Trabajador, Object> {

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "insert into trabajador (nombre, apellido, edad, salario, empresa, fk_entidad_id) values (:nombre, :apellido, :edad, :salario, :empresa, :entidad)",
            nativeQuery = true)
    int crearTrabajador(@Param("nombre") String nombre,
                        @Param("apellido") String apellido,
                        @Param("edad") Integer edad,
                        @Param("salario") String salario,
                        @Param("empresa") String empresa,
                        @Param("entidad") Integer entidad) throws Exception;

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "update trabajador set nombre =:nombre, apellido =:apellido, edad =:edad, salario =:salario, empresa =:empresa, fk_entidad_id =:entidad where id =:id",
            nativeQuery = true)
    int actualizarTrabajador(@Param("nombre") String nombre,
                             @Param("apellido") String apellido,
                             @Param("edad") Integer edad,
                             @Param("salario") String salario,
                             @Param("empresa") String empresa,
                             @Param("entidad") Integer entidad,
                             @Param("id") Integer id) throws Exception;

    @Query(value = "select * from trabajador where id =: id",
            nativeQuery = true)
    Trabajador encontrarTrabajador(@Param("id") Integer id) throws Exception;
}