/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.repository;

import com.recobro.incapacidades.entities.Incapacidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jeffersonecheverry
 */
@Repository("incapacidadRepository")
public interface IncapacidadRepository extends JpaRepository<Incapacidad, Object>{
    
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "insert into incapacidad (tipo_incapacidad, descripcion, auxilio) values (:tipoIncapacidad, :descripcion, :auxilio)",
            nativeQuery = true)
    int crearIncapacidad(@Param("tipoIncapacidad") String tipoIncapacidad,
                         @Param("descripcion") String descripcion,
                         @Param("auxilio") Integer auxilio) throws Exception;
    
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "update incapacidad set tipo_incapacidad =:tipoIncapacidad, descripcion =:descripcion, auxilio =:auxilio where id =:id",
            nativeQuery = true)
    int actualizarIncapacidad(@Param("tipoIncapacidad") String tipoIncapacidad,
                              @Param("descripcion") String descripcion,
                              @Param("auxilio") Integer Auxilio,
                              @Param("id") Integer id) throws Exception;
    
    @Query(value = "select * from incapacidad where id =:id",
            nativeQuery = true)
    Incapacidad encontrarIncapacidad(@Param("id") Integer id) throws Exception;
    
}
