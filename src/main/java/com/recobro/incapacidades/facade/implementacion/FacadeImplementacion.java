/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.facade.implementacion;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.recobro.incapacidades.facade.Facade;
import com.recobro.incapacidades.modelo.*;
import com.recobro.incapacidades.service.EntidadService;
import com.recobro.incapacidades.service.IncapacidadService;
import com.recobro.incapacidades.service.LoginService;
import java.util.logging.Logger;

import com.recobro.incapacidades.service.RecobroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;



/**
 *
 * @author jeffersonecheverry
 */
@Component("facade")
public class FacadeImplementacion implements Facade{
    
    private Gson gson = new Gson();
    private static final Logger LOGGER = Logger.getLogger(FacadeImplementacion.class.getName());
    
    private ResponseModelo responseModelo;
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private IncapacidadService incapacidadService;

    @Autowired
    private EntidadService entidadService;

    @Autowired
    private RecobroService recobroService;
    
    @Value("${EXITO}")
    private int exito;
    
    @Value("${FAIL}")
    private int fail;
    
    @Value("${MENSAJE_EXITO}")
    private String mensajeExitoso;
    
    @Value("${MENSAJE_FALLIDO}")
    private String mensajeFail;

    @Override
    public String operacion(String json) {
        JsonObject object = new JsonParser().parse(json).getAsJsonObject(); //El string que llega se para a jsonobject
        String operacion = object.get("operacion").getAsString() != null ? object.get("operacion").getAsString() : "";
        JsonObject body = object.get("body").getAsJsonObject() != null ? object.get("body").getAsJsonObject() : null;
        
        switch(operacion){
            case "LOGIN":{
                UsuarioModelo usuarioModelo = new UsuarioModelo();
                if(body != null){
                    usuarioModelo.setCorreo(body.get("correo").getAsString() != null ? body.get("correo").getAsString() : "");
                    usuarioModelo.setPassword(body.get("password").getAsString() != null ? body.get("password").getAsString() : "");
                    try{
                        responseModelo = new ResponseModelo(loginService.loginUsuario(usuarioModelo));
                        return gson.toJson(createResponseModelo(exito, mensajeExitoso, responseModelo));
                    }catch(Exception e){
                        LOGGER.info(e.getMessage());
                        responseModelo = new ResponseModelo(e.getMessage());
                        return gson.toJson(createResponseModelo(fail, mensajeFail, responseModelo));
                    }
                }
            }
            case "CREARENTIDAD" :{
                EntidadModelo entidadModelo = new EntidadModelo();
                if(body !=null){
                    entidadModelo.setNombre(body.get("nombre").getAsString() != null ? body.get("nombre").getAsString(): "");
                    entidadModelo.setDescripcion(body.get("descripcion").getAsString() != null ? body.get("descripcion").getAsString(): "");
                    entidadModelo.setPago(body.get("pago").getAsInt() > 0 ? body.get("pago").getAsInt(): 0);
                    try{
                        return gson.toJson(entidadService.crearEntidad(entidadModelo));
                    }catch (Exception e){
                        LOGGER.info(e.getMessage());
                    }
                }
            }
            case "CREARTRABAJADOR": {
                TrabajadorModelo trabajadorModelo = new TrabajadorModelo();
                if(body != null){
                    trabajadorModelo.setNombre(body.get("nombre").getAsString()!= null ? body.get("nombre").getAsString():"");
                    trabajadorModelo.setApellido(body.get("apellido").getAsString()!= null ? body.get("apellido").getAsString(): "");
                    trabajadorModelo.setEdad(body.get("edad").getAsInt()>0 ? body.get("edad").getAsInt(): 0);

                }
            }
            case "CONSULTARENTIDAD": {
                
            }
            case "CREARRECOBRO": {
                RecobroModelo recobroModelo = new RecobroModelo();
                if(body != null){
                    recobroModelo.setDescripcion(body.get("descripcion").getAsString() != null ? body.get("descripcion").getAsString(): "");
                    recobroModelo.setRemuneracion(body.get("remuneracion").getAsInt()>0 ? body.get("remuneracion").getAsInt(): 0);
                    recobroModelo.setIncapacidad(body.get("incapacidad").getAsInt()>0 ? body.get("incapacidad").getAsInt(): 0);
                    recobroModelo.setTrabajador(body.get("trabajador").getAsInt()> 0 ? body.get("trabajador").getAsInt(): 0);
                    recobroModelo.setEntidad(body.get("entidad").getAsInt()>0 ? body.get("entidad").getAsInt():0 );
                    try{
                        return gson.toJson(recobroService.crearRecobro(recobroModelo));
                    } catch (Exception e){
                        LOGGER.info(e.getMessage());
                    }

                }
                
            }
            case "CREARINCAPACIDAD": {
                IncapacidadModelo incapacidadModelo = new IncapacidadModelo();
                if(body != null){
                    incapacidadModelo.setTipoIncapacidad(body.get("tipoIncapacidad").getAsString() != null ? body.get("tipoIncapacidad").getAsString() : "");
                    incapacidadModelo.setDescripcion(body.get("inca_descripcion").getAsString()!= null ? body.get("inca_descripcion").getAsString(): "");
                    incapacidadModelo.setAuxilio(body.get("auxilio").getAsInt()>0 ? body.get("auxilio").getAsInt(): 0);
                    try{
                        return gson.toJson(incapacidadService.crearIncapacidad(incapacidadModelo));
                    }catch(Exception e){
                        LOGGER.info(e.getMessage());
                    }
                }
            }
        }
        
        return "";
    }
    
    private ResponseModelo createResponseModelo(int codigo, String mensaje, ResponseModelo responseModelo){
        responseModelo.setCodigoRetorno(codigo);
        responseModelo.setMensaje(mensaje);
        
        return responseModelo;
    }
    
}
