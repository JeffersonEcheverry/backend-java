/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recobro.incapacidades.facade;

/**
 *
 * @author jeffersonecheverry
 */
public interface Facade {
    
    String operacion(String json);
    
}
